#################################################
##
##      networks variables 
##  
################################################ 



variable "tags" {
  type    = map(string)
  default = null
}

variable "node_group_name" {
  type        = string
  default     = "demo-cluster-group"
  description = "resource group name"
}
variable "node_group_location" {
  type        = string
  default     = "West Europe"
  description = "resource group location"
}

