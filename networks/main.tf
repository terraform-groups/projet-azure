################################################################################
##
##
## 
################################################################################ 


resource "azurerm_resource_group" "this" {
  count    = var.create_resource_group ? 1 : 0
  name     = var.resource_group_name
  location = var.resource_group_location
  tags     = var.tags
}



resource "azurerm_network_security_group" "this" {
  count               = var.create_resource_group && var.create_network_security_group ? 1 : 0
  name                = var.security_group_name
  location            = azurerm_resource_group.this[0].location
  resource_group_name = azurerm_resource_group.this[0].name

  dynamic "security_rule" {
    for_each = { for k, v in var.rules : k => v }
    iterator = rule
    content {
      name                       = rule.name
      priority                   = rule.priority
      direction                  = rule.direction
      access                     = rule.access
      protocol                   = rule.protocol
      source_port_range          = try(rule.source_port_range, null)
      destination_port_range     = try(rule.destination_port_range, null)
      source_address_prefix      = try(rule.source_address_prefix, null)
      destination_address_prefix = try(rule.destination_address_prefix)
    }

  }

  tags = var.tags
}


resource "azurerm_virtual_network" "this" {
  count               = var.create_virtual_network && var.create_resource_group ? 1 : 0
  name                = var.virtual_network_name
  location            = azurerm_resource_group.this[0].location
  resource_group_name = azurerm_resource_group.this[0].name
  address_space       = var.address_space
}

resource "azurerm_subnet" "this" {
  for_each                    = { for k, v in var.subnets : k => v if(var.create_virtual_network && var.create_resource_group && var.create_subnet) }
  name                        = each.value.name
  resource_group_name         = azurerm_resource_group.this[0].name
  virtual_network_name        = azurerm_virtual_network.this[0].name
  address_prefixes            = each.value.address_prefixes
  service_endpoints           = lookup(each.value, "service_endpoints", null)
  service_endpoint_policy_ids = lookup(each.value, "service_endpoint_policy_ids", null)
  dynamic "delegation" {
    for_each = lookup(each.value, "delegation", null) != null ? [each.value.delegation] : []
    content {
      name = delegation.value.name
      service_delegation {
        for_each = lookup(delegation.value, "service_delegation", null) != null ? [delegation.value.service_delegation] : []
        content {
          name    = service_delegation.value.name
          actions = service_delegation.value.actions
        }
      }
    }
  }

}