# **<span style="color:FireBrick"> Terraform Module Networks:</span>**

![Alt text](../src/terraform-azure.svg)

[![Typing SVG](https://readme-typing-svg.demolab.com?font=Fira+Code&pause=1000&color=F73829&width=435&lines=Terraform++Azure+Module+Networks)](https://git.io/typing-svg)

![ligne_break](https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif)

## [![Typing SVG](https://readme-typing-svg.demolab.com?font=Fira+Code&pause=1000&color=F73829&repeat=false&width=435&lines=Contexte%3A)](https://git.io/typing-svg)

Le module permet de déployer les ressources ci-dessous: 


        ```mermaid
        graph TD;
        Resource_Group-->Security_Group;
        Security_Group-->Virtual_Network;
        Virtual_Network-->Subnets;
        ```



![ligne_break](https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif)

## **<span style="color:FireBrick"> Les Parametres:</span>**

### **<span style="color:FireBrick"> Ressource Group:</span>**

| Name    |  Description |  Values | Required |
|---------|--------------|---------|-------|
| name  | nom à attribuer à la ressource groupe  |  "" | yes |
| location  | la location pour la ressource group  | ""  | yes |
| tags  |  les tags |  {} | no |

![ligne_break](https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif)

### **<span style="color:FireBrick"> Security-Group:</span>**

| Name    |  Description |  Values | Required |
|---------|--------------|---------|-------|
| security_group_name  | nom   |  "" | yes |
| rules  |  les régles |{} [] | yes |

![ligne_break](https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif)

### **<span style="color:FireBrick"> Virtual-Network:</span>**

| Name    |  Description |  Values | Required |
|---------|--------------|---------|-------|
| virtual_network_name  |  nom à attribuer au vnet | ""  | yes |
| address_space  |  Plage d'ip |  [] | yes |

![ligne_break](https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif)

### **<span style="color:FireBrick"> Subnets:</span>**

| Name    |  Description |  Values | Required |
|---------|--------------|---------|-------|
| subnets  | nom du sous-réseau  |  "" | yes |

![ligne_break](https://user-images.githubusercontent.com/73097560/115834477-dbab4500-a447-11eb-908a-139a6edaec5c.gif)

### [![Typing SVG](https://readme-typing-svg.demolab.com?font=Fira+Code&pause=1000&color=F7531F&multiline=true&repeat=false&width=435&lines=Example+d'utilisation%3A)](https://git.io/typing-svg)

**<span style="color:FireBrick"> Usage Resource Group:</span>**

```terraform
module "networks" {
  source                  = "./"
  create_resource_group   = true
  resource_group_name     = "demo"
  resource_group_location = "West Europe"
  tags = {
    "Environment" = "Dev"
  }
  create_network_security_group = true
  security_group_name           = "demo-dev-sec"
  rules = {
    rules1 = {
      name                       = "test123"
      priority                   = 100
      direction                  = "Inbound"
      access                     = "Allow"
      protocol                   = "Tcp"
      source_port_range          = "*"
      destination_port_range     = "*"
      source_address_prefix      = "*"
      destination_address_prefix = "*"
    }
  }
  create_virtual_network = true
  virtual_network_name   = "test-vnet"
  address_space          = ["10.0.0.0/16"]
  create_subnet          = true
  subnets = {
    name                        = "example-subnet"
    address_space               = ["10.1.0.0/24"]
    service_endpoints           = [""]
    service_endpoint_policy_ids = [""]
    delegation = {
      name = "delegation"

      service_delegation = {
        name    = "Microsoft.ContainerInstance/containerGroups"
        actions = ["Microsoft.Network/virtualNetworks/subnets/join/action", "Microsoft.Network/virtualNetworks/subnets/prepareNetworkPolicies/action"]
      }
    }
  }
}


```


