output "resource_group_id" {
  value = azurerm_resource_group.this[0].id
}

output "virtual_network_id" {
  value = azurerm_virtual_network.this[0].id
}
output "subnets_id" {
  value = { for k, v in azurerm_subnet.this : k.id => v }
}

output "security_id" {
  value = azurerm_network_security_group.this[0].id
}