#################################################
##
##      networks variables 
##  
################################################ 
variable "create_resource_group" {
  type = bool
  description = "create resource group if true"
  default = true
}


variable "create_network_security_group" {
  type = bool
  description = "create security group if true and associate with resource group"
  default = false
}

variable "create_virtual_network" {
  type = bool
  description = "create virtual_network if true and associate with resource group"
  default = false
}
variable "create_subnet" {
  type = bool
  description = "create subnet if true and associate with virtual_network"
  default = false
}
variable "resource_group_name" {
  type = string
  default = "demo-cluster-group"
  description = "resource group name"
}
variable "resource_group_location" {
  type = string
  default = "West Europe"
  description = "resource group location"
}
variable "tags" {
  type = map(string)
  default = null
}


variable "security_group_name" {
  type = string
  default = "demo-cluster-security"
  description = "security group name"
}

variable "rules" {
  type = any
  default = null
  description = "rules of security groups"
}
variable "virtual_network_name" {
  type = string
  default = "demo-virtual-network"
  description = "virtual_network  name"
}
variable "address_space" {
  type = list(string)
  default = [ "10.0.0.0/16" ]
  description = "(Required) The address space that is used the virtual network. You can supply more than one address space."
}