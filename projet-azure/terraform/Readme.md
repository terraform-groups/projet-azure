
# **$\textcolor{FireBrick}{\text{TERRAFORM DEPLOY AKS}}$**

![Alt text](./src/terraform-azure.svg)
![Alt text](./src/jems.svg)


## **$\textcolor{FireBrick}{\text{Contexte:}}$**

L'objectif est de pouvoir déployer un cluster **$\textcolor{FireBrick}{\text{AKS}}$** avec les possibilités suivantes : 

- créer un cluster AKS par défault comme suite:
    1. Création d'un Vnet et subnet 
    2. Création d'une ressource groupe

        ```mermaid
        graph TD;
        Resource_Group-->Vnet;
        Vnet-->Subnet;
        Subnet-->AKS;
        ```

- créer un cluster Aks avec: 
    1. Possibilité de fournir ou pas une ressource groupe
    2. Fournissant un Vnet et/ou subnet

        ```mermaid
        graph TD;
        Subnet-->AKS;
        ```

## **$\textcolor{FireBrick}{\text{Modules:}}$**

les modules sont les suivants :

1. Resource Group
2. Vnet
3. Subnets
4. Dns
5. AKS 

## **$\textcolor{FireBrick}{\text{Les Parametres:}}$**

### **$\textcolor{FireBrick}{\text{Ressource Group:}}$**

| Name    |  Description |  Values | Required |
|---------|--------------|---------|-------|
| name  | nom à attribuer à la ressource groupe  |  "" | yes |
| location  | la location pour la ressource group  | ""  | yes |
| tags  |  les tags |  {} | no |

### **$\textcolor{FireBrick}{\text{Vnet:}}$**

| Name    |  Description |  Values | Required |
|---------|--------------|---------|-------|
| resource_group_name  | nom de la ressource groupe  |  "" | yes |
| resource_group_location  | la location de la ressource group  | ""  | yes |
| name  |  nom à attribuer au vnet | ""  | yes |
| address_space  |  Plage d'ip |  [] | yes |

### **$\textcolor{FireBrick}{\text{Subnets:}}$**

| Name    |  Description |  Values | Required |
|---------|--------------|---------|-------|
| name  | nom du sous-réseau  |  "" | yes |
| virtual_network_name  | nom du réseau virtual  | ""  | yes |
| resource_group_name  |  nom de la ressource group | ""  | yes |
| address_prefixes  |  Plage d'ip |  [] | yes |

### **$\textcolor{FireBrick}{\text{AKS:}}$**

| Name    |  Description |  Values | Required |
|---------|--------------|---------|-------|
| name  | nom du cluster  |  "" | yes |
| location  | location   | ""  | yes |
| resource_group_name  |  nom de la ressource group | ""  | yes |
| dns_prefix  | DNS prefix   | "" | no |
| sku_tier  | SKU Tier  | ""  | no |
| kubernetes_version  |   | ""  | yes |
| os_disk_size_gb  |  size of the OS Disk | ""  | no |
| node_resource_group  | name of the Resource Group   |   | yes |
| default_node_pool_name |pool_name | ""|yes |
| vm_size | size of the Virtual Machine|"" | yes|
| node_count | number of nodes| number | yes |
| admin_username | Admin Username| "" | yes|
| key_data |Public SSH Key | ""| yes|
| vnet_subnet_id | ID of the Subnet| ""|no |
| network_plugin |Network plugin | ""| yes|
| tags | mapping of tags| {}| no|

### **$\textcolor{FireBrick}{\text{Usages:}}$**

**<span style="color:White"> Usage Resource Group:</span>**

```terraform
module "resource_groups" {
  source = "../modules/ressource-groups"
  count = var.enabled  ? 1 : 0
  name = "mastergroups"
  location = "francecentral"
  tags = {
    "environment" = "test"
  }
}
```
**<span style="color:White"> Usage Vnet:</span>**

```terraform
module "vnet" {
  count = var.enabled ? 1 : 0
  source = "../modules/vnet"
  name = "virtual-net"
  resource_group_location = module.resource_groups[0].resource_group_location
  resource_group_name = module.resource_groups[0].resource_group_name
  address_space = [ "10.14.0.0/16" ]
}
```
**<span style="color:White"> Usage Subnet:</span>**

```terraform
module "subnet" {
  source = "../modules/subnets"
  count = var.enabled  ? 1 : 0
  address_prefixes = ["10.14.0.0/24"]
  virtual_network_name = module.vnet[0].virtual_network_name
  resource_group_name = module.resource_groups[0].resource_group_name
}
```
**<span style="color:White"> Usage AKS:</span>**

```terraform
module "aks" {
  source = "../modules/aks"
  name = "aks-test"
  resource_group_name = var.enabled == "true" ? module.resource_groups[0].resource_group_name : var.resource_group
  location = var.enabled == "true" ? module.resource_groups[0].resource_group_location  : var.location
  dns_prefix = var.enabled == "true" ?  "${module.resource_groups[0].resource_group_name}" :  "${var.resource_group}"
  sku_tier = "Free"
  kubernetes_version = "1.23.12"
  os_disk_size_gb = 30
  node_resource_group = var.enabled == "true" ? module.worker_groups[0].resource_group_name : var.resource_group
  default_node_pool_name = "defaultpool"
  vm_size = "Standard_D2_v2"
  node_count = 1
  admin_username = "ubuntu"
  key_data = file("${path.module}/ssh-key")
  vnet_subnet_id = var.enabled == "true" ? module.subnet[0].virtual_network_id : var.subnet_id
  network_plugin = "azure"
  tags = {
    "environment" = "Dev"
  }
  
}
```
**<span style="color:White"> Usage DNS:</span>**

```terraform
module "dns" {
  source = "../modules/dns"
  count = var.dns_enabled ? 1 : 0
  domain_name = "test.com"
  resource_group_name = var.enabled == "true" ? module.resource_groups[0].resource_group_name : var.resource_group
  dns_a_record = "test"
  target_resource_id = module.aks.id
  ttl = 300

}
```
## Contact

@: [MAHMOUD Mohamed](mmahmoud@jems-group.com)

Project Link: []


