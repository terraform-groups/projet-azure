# output public_ip {
#   value       = {for k in azurerm_public_ip.example : k.name => ({
#      ip_public = k.ip_address
#   }) }
# }
# output "ip" {
#   value = azurerm_public_ip.example["test"].ip_address
# }

output virtual_network_name {
  value       = azurerm_virtual_network.example.name
}
