variable resource_group_name {
  type        = string
  default     = "my-resource-group"
  description = "le nom de ta resource-groupe"
}

variable resource_group_location {
  type        = string
  default     = "West Europe"
  description = "la region ou la crée"
}


variable "name" {
  type = string
}                       
variable "address_space" {
  type = list(string)
}          

# variable public_ip {
#   type        = map(object({
#     name  = string
#     enabled = bool
#     allocation_method = string
#     tags = map(string)
#   }))
#   description = "description"
#   default = {
#     "test" = {
#       allocation_method = "Static"
#       enabled = true
#       name = "testpubliciP"
#       tags = {
#         "environment" = "dev"
#       }
#     }
#   }
# }


