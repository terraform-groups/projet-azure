resource "azurerm_kubernetes_cluster" "example" {
  name                = var.name
  location            = var.location
  resource_group_name = var.resource_group_name
  dns_prefix          = var.dns_prefix
  kubernetes_version  = var.kubernetes_version
  node_resource_group = var.node_resource_group
  sku_tier                            = var.sku_tier
  default_node_pool {
    name       = var.default_node_pool_name
    node_count = var.node_count
    vm_size    = var.vm_size
    os_disk_size_gb = var.os_disk_size_gb
    vnet_subnet_id = var.vnet_subnet_id
    tags = var.tags
  }
  linux_profile {
    admin_username = var.admin_username
    ssh_key {
      key_data = var.key_data
    }
  }
  identity {
    type = "SystemAssigned"
  }
  network_profile {
    network_plugin     = var.network_plugin
  }
  tags = var.tags
}