variable "name" {
    type = string
}
variable "location" {
  type = string
}
variable "resource_group_name" {
  type = string
}
variable "dns_prefix" {
  type = string
}
variable "kubernetes_version" {
  type = string
}
variable "node_resource_group" {
  type = string
}
variable "default_node_pool_name" {
  type = string
}
variable "node_count" {
  type = number
}
variable "vm_size" {
  type = string
}
variable "os_disk_size_gb" {
  type = number
}
variable "vnet_subnet_id" {
  type = string
}
variable "tags" {
  type = map(string)
}
variable "sku_tier" {
  
}
variable "admin_username" {
  type = string
}
variable "key_data" {
  
}
variable "network_plugin" {
  
}

# variable "net_profile_dns_service_ip" {
  
# }
# variable "net_profile_docker_bridge_cidr" {
  
# }
# variable "network_policy" {
  
# }
# variable "net_profile_service_cidr" {
  
# }
