variable "domain_name" {
  type = string
  default = "domain.com"
}
variable "resource_group_name" {
  type = string 
}
variable "dns_a_record" {
  type = string 
  default = "test"
}

variable "records" {
  type = list(string)
  default = [ "10.0.180.17" ]
}
variable "ttl" {
  type = number 
  default = 300
}
variable "target_resource_id" {
  
}