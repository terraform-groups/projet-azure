resource "azurerm_dns_zone" "example-public" {
  name                = var.domain_name
  resource_group_name = var.resource_group_name 
}

resource "azurerm_dns_a_record" "example" {
  name                = var.dns_a_record
  zone_name           = azurerm_dns_zone.example-public.name
  resource_group_name = var.resource_group_name
  ttl                 = var.ttl
  target_resource_id = var.target_resource_id
}