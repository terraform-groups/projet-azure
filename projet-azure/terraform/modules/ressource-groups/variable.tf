
variable "name" {
  type = string
  default = "test"
}

variable "location" {
  type = string
  default = "francecentral"
}
variable "tags" {
  type = map(string)
  default = {
    "enviroment" = "test"
  }
}