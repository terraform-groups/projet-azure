variable resource_group_name {
  type        = string
  default     = "my-resource-rg"
  description = "le nom de ta resource-groupe"
}

variable virtual_network_name {
  type        = string
  default     = "virtual-vnet"
  description = "description"
}
variable "address_prefixes" {
  default = ["10.0.2.0/24"]
}



variable name {
  type = string 
  default = "test"
}
