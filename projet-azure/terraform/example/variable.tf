variable "enabled" {
  type = string
  default = true
}
variable "resource_group" {
  default = ""
}
variable "location" {
  default = ""
}
variable "subnet_id" {
  default = ""
}
variable "dns_enabled" {
  default = false
  
}