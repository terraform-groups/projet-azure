module "resource_groups" {
  source = "../modules/ressource-groups"
  count = var.enabled  ? 1 : 0
  name = "mastergroups"
  location = "francecentral"
  tags = {
    "environment" = "test"
  }
}
module "worker_groups" {
  source = "../modules/ressource-groups"
  count = var.enabled  ? 1 : 0
  name = "workgroups"
  location = "francecentral"
  tags = {
    "environment" = "test"
  }
}
module "vnet" {
  count = var.enabled ? 1 : 0
  source = "../modules/vnet"
  name = "virtual-net"
  resource_group_location = module.resource_groups[0].resource_group_location
  resource_group_name = module.resource_groups[0].resource_group_name
  address_space = [ "10.14.0.0/16" ]
}
module "subnet" {
  source = "../modules/subnets"
  count = var.enabled  ? 1 : 0
  address_prefixes = ["10.14.0.0/24"]
  virtual_network_name = module.vnet[0].virtual_network_name
  resource_group_name = module.resource_groups[0].resource_group_name
}
module "aks" {
  source = "../modules/aks"
  name = "aks-test"
  resource_group_name = var.enabled == "true" ? module.resource_groups[0].resource_group_name : var.resource_group
  location = var.enabled == "true" ? module.resource_groups[0].resource_group_location  : var.location
  dns_prefix = var.enabled == "true" ?  "${module.resource_groups[0].resource_group_name}" :  "${var.resource_group}"
  sku_tier = "Free"
  kubernetes_version = "1.23.12"
  os_disk_size_gb = 30
  node_resource_group = var.enabled == "true" ? module.worker_groups[0].resource_group_name : var.resource_group
  default_node_pool_name = "defaultpool"
  vm_size = "Standard_D2_v2"
  node_count = 1
  admin_username = "ubuntu"
  key_data = file("${path.module}/ssh-key")
  vnet_subnet_id = var.enabled == "true" ? module.subnet[0].virtual_network_id : var.subnet_id
  network_plugin = "azure"
  tags = {
    "environment" = "Dev"
  }
  
}

module "dns" {
  source = "../modules/dns"
  count = var.dns_enabled ? 1 : 0
  domain_name = "test.com"
  resource_group_name = var.enabled == "true" ? module.resource_groups[0].resource_group_name : var.resource_group
  dns_a_record = "test"
  target_resource_id = module.aks.id
  ttl = 300

}